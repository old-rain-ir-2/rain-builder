/*!
The main `rain` repl
*/
use super::*;
use rain_builder::Builder;

/// A repl command
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Command {
    /// A `rain` expression
    Expr(Expr),
}

/// Attempt to parse a repl command
pub fn command(input: &str) -> IResult<&str, Command> {
    map(expr, Command::Expr)(input)
}

/// The repl state
#[derive(Default)]
pub struct Repl {
    /// Whether to debug parse states
    pub debug_parse: bool,
    /// The builder associated with this repl
    pub builder: Builder,
}

impl Repl {
    /// Create a new repl
    pub fn new() -> Repl {
        Repl::default()
    }
    /// Handle a line as input to the repl
    pub fn handle_line<'a>(&mut self, line: &'a str) -> IResult<&'a str, ()> {
        let (rest, command) = command(line)?;
        println!("Parsed command: {:#?}", command);
        match &command {
            Command::Expr(expr) => match self.builder.build_expr(expr) {
                Ok(value) => println!("Built value: {:#?}", value),
                Err(err) => println!("Error building value: {}", err),
            },
        }
        println!("Leftover input: {}", rest);
        Ok((rest, ()))
    }
}
