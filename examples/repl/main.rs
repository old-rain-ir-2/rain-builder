/*!
A simple REPL for `rain` IR in the standard format
*/

use anyhow;
use nom::combinator::{map, opt};
use nom::multi::many0_count;
use nom::sequence::terminated;
use nom::IResult;
use rain_ast::ast::Expr;
use rain_ast::parser::{expr, ws};
use rustyline::error::ReadlineError;
use rustyline::hint::{Hinter, HistoryHinter};
use rustyline::validate::{ValidationContext, ValidationResult, Validator};
use rustyline::{Context, Editor};
use rustyline_derive::{Completer, Helper, Highlighter};

mod helper;
mod repl;
pub use helper::*;
pub use repl::*;

const HISTORY: &str = ".rain_history";
const PROMPT: &str = ">>> ";

fn main() -> anyhow::Result<()> {
    let mut editor = Editor::<RainValidator>::new();
    if editor.load_history(HISTORY).is_err() {
        eprintln!("No previous history loaded.")
    }
    let validator = RainValidator::new();
    editor.set_helper(Some(validator));
    let mut repl = Repl::new();
    loop {
        match editor.readline(PROMPT) {
            Ok(line) => match repl.handle_line(&line) {
                Ok(_) => {
                    editor.add_history_entry(&line);
                }
                Err(err) => eprintln!("REPL error: {}", err),
            },
            Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => break,
            Err(err) => eprintln!("IO error: {:#?}", err),
        }
    }
    editor.save_history(HISTORY)?;
    Ok(())
}
