/*!
A helper for the `rain` repl
*/
use super::*;

/// A validator for `rain` input code.
#[derive(Completer, Helper, Highlighter)]
pub struct RainValidator {
    hinter: HistoryHinter,
}

impl RainValidator {
    pub fn new() -> RainValidator {
        RainValidator {
            hinter: HistoryHinter {},
        }
    }
}

impl Validator for RainValidator {
    fn validate(&self, ctx: &mut ValidationContext) -> Result<ValidationResult, ReadlineError> {
        match terminated(many0_count(command), opt(ws))(ctx.input()) {
            Ok(("", _n)) => Ok(ValidationResult::Valid(None)),
            Ok((_rest, _n)) => Ok(ValidationResult::Incomplete),
            Err(err) => Ok(ValidationResult::Invalid(Some(format!(
                "Parse error: {:#?}",
                err
            )))),
        }
    }
}

impl Hinter for RainValidator {
    fn hint(&self, line: &str, pos: usize, ctx: &Context<'_>) -> Option<String> {
        self.hinter.hint(line, pos, ctx)
    }
}
