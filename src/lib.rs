/*!
# rain-builder

A tool to convert a `rain` AST into `rain` IR. As a usage example, a simple REPL based off this tool is provided.
*/
#![forbid(unsafe_code, missing_docs, missing_debug_implementations)]
#![warn(clippy::all)]
use failure::Fail;
use num::BigUint;
use rain_ast::ast::Expr;
use rain_ir::error::Error as IRError;
use rain_ir::primitive::logical::*;
use rain_ir::primitive::nats::*;
use rain_ir::value::*;

/// A struct which converts a `rain` AST into `rain` IR, maintaining a context for variable names and metadata
#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct Builder {}

/// An error while building `rain` IR
#[derive(Fail, Debug, Clone, Eq, PartialEq)]
pub enum Error {
    /// An error building `rain` IR
    #[fail(display = "IR Error: {}", 0)]
    IR(IRError),
}

impl From<IRError> for Error {
    fn from(err: IRError) -> Error {
        Error::IR(err)
    }
}

impl Builder {
    /// Create a new builder
    #[inline]
    pub fn new() -> Builder {
        Builder::default()
    }
    /// Build an expression in the current context
    pub fn build_expr(&mut self, expr: &Expr) -> Result<ValId, Error> {
        let result = match expr {
            Expr::Natural(n) => self.build_nat(n).into_val(),
            Expr::Nats => NATS.clone_as_val(),
            Expr::Finite => FINITE.clone_as_val(),
            Expr::Boolean(true) => TRUE.clone_as_val(),
            Expr::Boolean(false) => FALSE.clone_as_val(),
            Expr::Bool => BOOL.clone_as_val(),
            e => unimplemented!("Building expression: {:#?}", e),
        };
        Ok(result)
    }
    /// Build a natural number in the current context
    #[inline]
    pub fn build_nat(&mut self, nat: &BigUint) -> VarId<Natural> {
        VarId::from_var(Natural::new(nat.clone()))
    }
}
